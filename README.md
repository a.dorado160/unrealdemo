Instrucciones de ejecución: 

Se instaló en el computador hp omen.

Método 1:

Para la ejecución de la demostración se puede clonar este repositorio (sin la carpeta bin) y abrir el archivo MyProject.uproject en Unreal Engine 4.21.2; El mapa de la demostración esta en /Maps/demostration. Después de cargado todos los elementos y compilado todos los shaders, se procede a correr la demostración en modo VR.

Método 2:

https://uniandes-my.sharepoint.com/:f:/g/personal/a_dorado160_uniandes_edu_co/EqNxxyTSMP1LiLElHGblg7IBnR4EY0R3gkr7u9GZTmYSWg?e=IGxK1g

Se procede a bajar solamente la carpeta bin de la URL, el cual cuenta con el ejecutable de la demostración. Simplemente es ejecutar el archivo .exe que llevara directamente al mapa de la demostración.